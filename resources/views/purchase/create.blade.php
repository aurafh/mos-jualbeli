<form id="FormData" method="POST">
    @csrf
    <input type="hidden" id="id_purchase" name="id_purchase" value="">
    <div class="row mb-3">
        <label for="number" class="col-sm-4 col-form-label"><b>Code</b></label>
        <div class="col-sm-8 numberInput">
            {{-- <input type="text" class="form-control" id="number" name="number" value="" disabled> --}}
        </div>
    </div>
    <div class="row mb-3">
        <label for="date" class="col-sm-4 col-form-label"><b>Date</b></label>
        <div class="col-sm-8 dateInput">
            {{-- <input type="date" class="form-control" id="date" name="date" value=""> --}}
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table-striped" id="general_comments">
            <thead>
                <tr>
                    <th width="30%">Name</th>
                    <th width="20%">Stock</th>
                    <th width="15%">Price</th>
                    <th width="15%">Qty</th>
                    <th width="15%">Total</th>
                    <th width="5%" id=thButton>
                        {{-- <button class="btn btn-success" type="button" id="btn-add-row">ADD</button> --}}
                    </th>
                    </tr>
            </thead>
            <tbody id="table-content">
                
            </tbody>
            </table>
        </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4"><b>Total Price </b></div>
        <div class="col-sm-8 totalPrice">
            {{-- <input type="number" class="form-control" id="totalPrice" readonly disabled> --}}
        </div>
    </div>
    <div id="saveBtn">
        {{-- <button class="btn btn-primary" type="submit">SAVE</button> --}}
    </div>
</form>

