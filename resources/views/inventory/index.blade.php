@extends('partials.main')

@section('content')
    <div class="main bg-light py-3">
        <div class="container">
            <div class="card">
                <div class="card-body">
                    <div class="card-title py-2">
                        <h5>Data Inventori</h5>
                    </div>
                    <div class="card-description">
                        <div class="row">
                            <div class="col mb-3">
                                <button class="btn btn-primary" id="add-button">Tambah Data</button>
                            </div>
                        </div>
                    </div>
                    <div class="table-responssive"id="readData">
                        {{-- Table Data Inventory --}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="createModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="ModalLabel">Modal title</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div id="page">
                        {{-- Form --}}
                        @include('inventory.create')
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Edit-->
    <div class="modal fade" id="createModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="ModalLabel">Modal title</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div id="page">
                        {{-- Form --}}
                        @include('inventory.create');
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script>
        $(document).ready(function() {
            $("#add-button").on('click', function() {
            $("#code").val('');
            $("#name").val('');
            $("#price").val('');
            $("#stock").val('');
            $("#createModal").modal('show');
            $("#ModalLabel").text('Tambah Data Inventory');
        });
        });

        //read data
        function read() {
            $.get("{{ url('baca') }}", {}, function(data, status) {
                $("#readData").html(data);
                $('#myTable').DataTable({
                    dom: 'Bfrtip',
                    buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                });
            });
        }


        //proses show edit
        function show(id) {
            $.ajax({
            url: 'inventory/'+ id+'/edit',
            type: 'GET',
            dataType: 'JSON',
            success: function(data, textStatus, jqXHR) {
            var inventory = data.inventori;

            $('#id_inventory').val(inventory.id);
            $('#code').val(inventory.code);
            $('#name').val(inventory.name);
            $('#price').val(inventory.price);
            $('#stock').val(inventory.stock);
            $('formButton').html('UPDATE');
            $("#ModalLabel").html("Edit Data Inventori")
            $("#createModal").modal('show');

            }
        });
    };

        //proses delete data
        function deleteData(itemID) {
            var itemId = $(this).data('id');
            Swal.fire({
                title: 'Hapus Data',
                text: 'Apakah Anda yakin ingin menghapus data ini?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "POST",
                        url: "{{ url('/inventory') }}/" + itemID,
                        data: {
                            _token: "{{ csrf_token() }}",
                            _method: "DELETE"
                        },
                        success: function(data) {
                            $(".btn-close").click();
                            Swal.fire('Data Terhapus', '', 'success');
                            read()
                        }
                    });
                }
            });
        };


$(document).ready(function() {
    $('#FormData').submit(function(e) {
        e.preventDefault();

        var formData = $(this).serialize();
        var inventoryID = $('#id_inventory').val();
        
        if(inventoryID){
            $.ajax({
                type: 'PUT',
                url: '/inventory/' + inventoryID,
                data: formData,
                dataType: 'json',
                success: function(data) {
                    Swal.fire({
                        title: 'Sukses',
                        text: 'Data Inventori berhasil diubah.',
                        icon: 'success',
                    }).then(function() {
                        $(".btn-close").click();
                        read();
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'Terjadi kesalahan saat memperbarui data.',
                });
            },
            });

        } else{
            $.ajax({
                type: "POST",
                url: '/inventory',
                data: formData,
                dataType: 'json',
                success: function(data) {
                    Swal.fire({
                        title: 'Sukses',
                        text: 'Data Inventori berhasil disimpan.',
                        icon: 'success',
                    }).then(function() {
                        $(".btn-close").click();
                        read();
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'Terjadi kesalahan saat menyimpan data.',
                });
            },
            });
        }

    });
});
    
</script>
@endsection