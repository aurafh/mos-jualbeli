<form id="FormData" method="POST">
  @csrf
  <input type="text" class="form-control" id="id_inventory" name="id_inventory" value="" hidden>
  <div class="row mb-3">
  <label for="code" class="col-sm-4 col-form-label" hidden>Kode Barang</label>
  <div class="col-sm-8">
    <input type="text" class="form-control" id="code" name="code" value="" disabled hidden>
  </div>
</div>
<div class="row mb-3">
  <label for="name" class="col-sm-4 col-form-label">Nama Barang</label>
  <div class="col-sm-8">
    <input type="text" class="form-control" id="name" name="name" value="">
  </div>
</div>
<div class="row mb-3">
  <label for="price" class="col-sm-4 col-form-label">Price</label>
  <div class="col-sm-8">
    <input type="number" class="form-control" id="price" name="price" value="">
  </div>
</div>
<div class="row mb-4">
  <label for="stock" class="col-sm-4 col-form-label">Stock</label>
  <div class="col-sm-8">
    <input type="number" class="form-control" id="stock" name="stock" value="">
  </div>
</div>
<div class="row">
  <div class="col-1">
    <button class="btn btn-primary" id="formButton">Simpan</button>
  </div>
</div>
</form>
